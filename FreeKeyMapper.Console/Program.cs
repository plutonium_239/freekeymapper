﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeKeyMapper.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                PrintHelp();
                return;
            }

            if (args[0] == "-l")
            {
                PrintMappings();

                Console.Read();
            }
            else if(args[0] == "-add")
            {
                if (args.Count() < 3)
                    throw new ArgumentException("Invalid argument count");

                string codeFrom = args[1];
                string codeTo = args[2];

                KeyMapper.AppendMapping(codeFrom, codeTo);

                //var mappings = KeyMapper.LoadMappings();
                //KeyMapper.SaveMappings(mappings);

                Console.WriteLine(String.Format("Added mapping from {0} to {1}", codeFrom, codeTo));

                Console.Read();
            }
            else if(args[0] == "-del")
            {
                if (args.Count() < 2)
                    throw new ArgumentException("Invalid argument count");

                int indexToDel = Int32.Parse(args[1]);

                KeyMapper.DeleteMapping(indexToDel);

                Console.WriteLine("Mapping deleted. Current mappings:");
                PrintMappings();
            }
            else
                PrintHelp();
        }

        private static void PrintHelp()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("-l - List all mappings");
            Console.WriteLine("-add [key code from] [key code to] - Add new mapping");
            Console.WriteLine("-del [index] - Delete the entry from list (can be seen with -l)");
            Console.Read();
        }

        private static void PrintMappings()
        {
            var mappings = KeyMapper.LoadMappingsPresentation();

            Console.WriteLine();

            foreach (var mapping in mappings)
            {
                Console.WriteLine(String.Format("{2}:{0} -> {1}", mapping.MapFromName, mapping.MapToName, mapping.Index));
            }
        }
    }
}
