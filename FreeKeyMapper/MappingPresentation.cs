﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeKeyMapper
{
    public class MappingPresentation : Mapping
    {
        public string MapFromName { get; set; }

        public string MapToName { get; set; }
    }
}
