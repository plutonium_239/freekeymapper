﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeKeyMapper
{
    public class Mapping
    {
        public int Index { get; set; }

        public string MapFromCode { get; set; }

        public string MapToCode { get; set; }
    }
}
