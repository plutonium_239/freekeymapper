﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeKeyMapper
{
    public static class KeyHelper
    {
        private static readonly Dictionary<string, string> DecKeys = new Dictionary<string, string>()
        {
            {"00_00", "-- Key disabled" },

            {"00_01", "Special: Escape"},
            {"00_02", "Key: 1 !"},
            {"00_03", "Key: 2 @"},
            {"00_04", "Key: 3 #"},
            {"00_05", "Key: 4 $"},
            {"00_06", "Key: 5 %"},
            {"00_07", "Key: 6 ^"},
            {"00_08", "Key: 7 &"},
            {"00_09", "Key: 8 *"},
            {"00_0A", "Key: 9 {"},
            {"00_0B", "Key: 0 }"},
            {"00_0C", "Key: - _"},
            {"00_0D", "Key: = +"},
            {"00_0E", "Special: Backspace"},
            {"00_0F", "Special: Tab"},
            {"00_10", "Key: Q"},
            {"00_11", "Key: W"},
            {"00_12", "Key: E"},
            {"00_13", "Key: R"},
            {"00_14", "Key: T"},
            {"00_15", "Key: Y"},
            {"00_16", "Key: U"},
            {"00_17", "Key: I"},
            {"00_18", "Key: O"},
            {"00_19", "Key: P"},
            {"00_1A", "Key: [ {"},
            {"00_1B", "Key: ] }"},
            {"00_1C", "Special: Enter"},
            {"00_1D", "Special: Left Ctrl"},
            {"00_1E", "Key: A"},
            {"00_1F", "Key: S"},
            {"00_20", "Key: D"},
            {"00_21", "Key: F"},
            {"00_22", "Key: G"},
            {"00_23", "Key: H"},
            {"00_24", "Key: J"},
            {"00_25", "Key: K"},
            {"00_26", "Key: L"},
            {"00_27", "Key: , :"},
            {"00_28", "Key: ' \""},
            {"00_29", "Key: ` ~"},
            {"00_2A", "Special: Left Shift"},
            {"00_2B", "Key: \\ |"},
            {"00_2C", "Key: Z"},
            {"00_2D", "Key: X"},
            {"00_2E", "Key: C"},
            {"00_2F", "Key: V"},
            {"00_30", "Key: B"},
            {"00_31", "Key: N"},
            {"00_32", "Key: M"},
            {"00_33", "Key: , <"},
            {"00_34", "Key: . >"},
            {"00_35", "Key: / ?"},
            {"00_36", "Special: Right Shift"},
            {"00_37", "Num: *"},
            {"00_38", "Special: Left Alt"},
            {"00_39", "Special: Space"},
            {"00_3A", "Special: Caps Lock"},
            {"00_3B", "Function: F1"},
            {"00_3C", "Function: F2"},
            {"00_3D", "Function: F3"},
            {"00_3E", "Function: F4"},
            {"00_3F", "Function: F5"},
            {"00_40", "Function: F6"},
            {"00_41", "Function: F7"},
            {"00_42", "Function: F8"},
            {"00_43", "Function: F9"},
            {"00_44", "Function: F10"},
            {"00_45", "Special: Num Lock"},
            {"00_46", "Special: Scroll Lock"},
            {"00_47", "Num: 7"},
            {"00_48", "Num: 8"},
            {"00_49", "Num: 9"},
            {"00_4A", "Num: -"},
            {"00_4B", "Num: 4"},
            {"00_4C", "Num: 5"},
            {"00_4D", "Num: 6"},
            {"00_4E", "Num: +"},
            {"00_4F", "Num: 1"},
            {"E0_40", "F-Lock: Close"},       //   F6
            {"E0_41", "F-Lock: Reply"},       //   F7
            {"E0_42", "F-Lock: Fwd"},         //   F8
            {"E0_43", "F-Lock: Send"},        //   F9
            {"E0_44", "Unknown: 0xE044"},
            {"E0_45", "Special: €"},
            {"E0_46", "Unknown: 0xE046"},
            {"E0_47", "Special: Home"},
            {"E0_48", "Arrow: Up"},
            {"E0_49", "Special: Page Up"},
            {"E0_4A", "Unknown: 0xE04A"},
            {"E0_4B", "Arrow: Left"},
            {"E0_4C", "Unknown: 0xE04C"},
            {"E0_4D", "Arrow: Right"},
            {"E0_4E", "Unknown: 0xE04E"},
            {"E0_4F", "Special: End"},
            {"E0_50", "Arrow: Down"},
            {"E0_51", "Special: Page Down"},
        };

        public static string GetByCode(string code)
        {
            string value = "Unknown key:" + code;
            bool hasResult = DecKeys.TryGetValue(code, out value);

            return value;
        }

        public static KeyValuePair<string,string> GetByName(string name)
        {
            var res = DecKeys.FirstOrDefault(k => k.Value == name);

            return res;
        }
    }
}
