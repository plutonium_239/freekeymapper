﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeKeyMapper
{
    public static class KeyMapper
    {
        //private static readonly List<Mapping> _mappings = new List<Mapping>();

        private const int MAPPING_OFFSET = 12;

        private static List<Mapping> LoadMappings()
        {
            var mappings = new List<Mapping>();

            RegistryKey regScanMapKey = Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Control\\Keyboard Layout");
            if (regScanMapKey == null)
                throw new Exception("Invalid registry key");

            byte[] keyMappingsBytes = (byte[])regScanMapKey.GetValue("Scancode Map");
            if(keyMappingsBytes == null)
            {
                regScanMapKey.Close();
                return new List<Mapping>();
            }

            //For some weird reason, it's bigger by 1 than the actual mapping count
            int totalMappingsCount = keyMappingsBytes[8];

            for(int i=0; i<totalMappingsCount-1; i++)
            {
                string mapFromCode = string.Format("{0,2:X}_{1,2:X}", keyMappingsBytes[(i * 4) + 12 + 3], keyMappingsBytes[(i * 4) + 12 + 2]);
                string mapToCode = string.Format("{0,2:X}_{1,2:X}", keyMappingsBytes[(i * 4) + 12 + 1], keyMappingsBytes[(i * 4) + 12 + 0]);

                MappingPresentation currentMapping = new MappingPresentation() { Index = i, MapFromCode = mapFromCode, MapToCode = mapToCode };

                mappings.Add(currentMapping);
            }
            return mappings;
        }

        public static List<MappingPresentation> LoadMappingsPresentation()
        {
            var mappings = LoadMappings();

            var res = new List<MappingPresentation>();

            for(int i =0; i<mappings.Count(); i++)
            {
                res.Add(new MappingPresentation()
                {
                    Index = i,
                    MapFromName = KeyHelper.GetByCode(mappings.ElementAt(i).MapFromCode),
                    MapToName = KeyHelper.GetByCode(mappings.ElementAt(i).MapToCode)
                });
            }

            return res;
        }

        public static void SaveMappings(List<Mapping> mappings)
        {
            RegistryKey regScanMapKey = Registry.LocalMachine.CreateSubKey("System\\CurrentControlSet\\Control\\Keyboard Layout");
            if (regScanMapKey == null)
                throw new Exception("Can't create or open the registry key");

            if(mappings.Count == 0)
            {
                regScanMapKey.DeleteValue("Scancode map", false);
                regScanMapKey.Close();
                return;
            }

            //8 bytes empty, 4 for mappings count, 4 for each mapping, 4 bytes in the end for unknown purpose
            byte[] keyMappingBytes = new byte[8 + 4 + (4 * mappings.Count) + 4];
            keyMappingBytes[8] = Convert.ToByte(mappings.Count + 1);

            for(int i =0; i < mappings.Count; i++)
            {
                var mappingArr = mappings[i].MapToCode.Split('_');
                keyMappingBytes[(i * 4) + MAPPING_OFFSET + 0] = Convert.ToByte(mappingArr[1], 16);
                keyMappingBytes[(i * 4) + MAPPING_OFFSET + 1] = Convert.ToByte(mappingArr[0], 16);

                mappingArr = mappings[i].MapFromCode.Split('_');
                keyMappingBytes[(i * 4) + MAPPING_OFFSET + 2] = Convert.ToByte(mappingArr[1], 16);
                keyMappingBytes[(i * 4) + MAPPING_OFFSET + 3] = Convert.ToByte(mappingArr[0], 16);
            }

            regScanMapKey.SetValue("Scancode map", keyMappingBytes);

            regScanMapKey.Close();
        }

        public static void AppendMapping(string codeFrom, string codeTo)
        {
            var mappings = LoadMappings();

            mappings.Add(new Mapping() { MapFromCode = codeFrom, MapToCode = codeTo });
            SaveMappings(mappings);
        }

        public static void DeleteMapping(int index)
        {
            var mappings = LoadMappings();

            if (mappings.Count - 1 > index)
                throw new ArgumentException("Invalid index");

            mappings.RemoveAt(index);
            SaveMappings(mappings);
        }
    }
}
