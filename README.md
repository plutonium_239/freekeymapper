# FreeKeyMapper

Remap keyboard keys on your Windows machine.

It changes the mappings with the help of Registry, so you need admin priviedges for it to work, as well as restart afterwards.

## Motivation
Sure, this is not the first tool to do it. But, I found surprisingly small amount of tools capable to do it, even less are open-source.
Best one seems to be SharpKeys, but it's WinForms and not really extendable. So I decided to make this one.